/* 1. Вывести список самолетов с кодами 320, 321, 733 */
select a.model, a.aircraft_code 
from bookings.aircrafts a 
where a.aircraft_code in('320', '321', '733');

/* 2. Вывести список самолетов с кодом не на 3 */
select a.model, a.aircraft_code 
from bookings.aircrafts a
where a.aircraft_code not like '3%';

/* 3. Найти билеты оформленные на имя «OLGA», с емайлом «OLGA» или без емайла */
select * 
from bookings.tickets t 
where t.passenger_name like 'OLGA%'
	  and (t.contact_data ->> 'email') is null
	  or upper(t.contact_data ->> 'email') like 'OLGA%';
	  
/* 4. Найти самолеты с дальностью полета 5600, 5700. 
   Отсортировать список по убыванию дальности полета */
select a.model, a."range" 
from bookings.aircrafts a 
where a."range" in(5600, 5700)
order by a."range" desc;

/* 5. Найти аэропорты в Moscow. Вывести название аэропорта вместе с городом.
   Отсортировать по полученному названию */
select a.airport_name, a.city 
from bookings.airports a 
where a.city = 'Москва'
order by a.airport_name;

/* 6. Вывести список всех городов без повторов в зоне «Europe» */
select distinct a.city, a.timezone  
from bookings.airports a
where a.timezone like 'Europe%';

/* 7. Найти бронирование с кодом на «3A4» и вывести сумму брони со скидкой 10% */
select b.book_ref, b.total_amount, b.total_amount * 0.9 as Бронь_со_скидкой
from bookings.bookings b
where b.book_ref like '3A4%';

/* 8. Вывести все данные по местам в самолете с кодом 320 и классом «Business»
    строками вида «Данные по месту: номер места 1», «Данные по месту: номер места 2» … и тд */
select concat('Данные по месту: номер места ', s.seat_no) as Данные_по_местам 
from bookings.seats s
where s.aircraft_code = '320'
	  and s.fare_conditions = 'Business';
	 
/* 9. Найти максимальную и минимальную сумму бронирования в 2017 году */
select max(b.total_amount) as Максимальная_сумма_брони,
	   min(b.total_amount) as Минимальная_сумма_брони
from bookings.bookings b 
where EXTRACT(YEAR FROM b.book_date) = 2016; --2017 года нет в базе, взял по 2016

/* 10. Найти количество мест во всех самолетах, вывести в разрезе самолетов */
select a.model, count(s.seat_no) as Количество_мест_в_самолете
from bookings.aircrafts a 
	 join bookings.seats s on a.aircraft_code = s.aircraft_code 
group by a.model;

/* 11. Найти количество мест во всех самолетах с учетом типа места,
   вывести в разрезе самолетов и типа мест */
select a.model, s.fare_conditions, count(s.seat_no) as Количество_мест_в_самолете
from bookings.aircrafts a 
	 join bookings.seats s on a.aircraft_code = s.aircraft_code 
group by a.model, s.fare_conditions
order by a.model asc;

/* 12. Найти количество билетов пассажира ALEKSANDR STEPANOV, телефон которого заканчивается на 11 */
select count(t.ticket_no) as Количество_билетов 
from bookings.tickets t 
where t.passenger_name = 'ALEKSANDR STEPANOV'
	  and t.contact_data ->> 'phone' like '%11';
	  
/* 13. Вывести всех пассажиров с именем ALEKSANDR, у которых количество билетов больше 2000. 
   Отсортировать по убыванию количества билетов */
select t.passenger_name, count(t.ticket_no) as Количество_билетов 
from bookings.tickets t 
where t.passenger_name like 'ALEKSANDR %'
group by t.passenger_name 
having count(t.ticket_no) > 2000
order by Количество_билетов desc;

/* 14. Вывести дни в сентябре 2017 с количеством рейсов больше 500 */
select EXTRACT(day FROM f.actual_departure) as День_в_сентябре,
	   count(f.flight_id) as Количество_рейсов
from bookings.flights f 
where EXTRACT(year FROM f.actual_departure) = 2016 --2017 года нет в базе, взял по 2016
	  and EXTRACT(month FROM f.actual_departure) = 9
group by День_в_сентябре
having count(f.flight_id) > 500;

/* 15. Вывести список городов, в которых несколько аэропортов */
select a.city 
from bookings.airports a
group by a.city 
having  count(a.airport_code) >= 2;

/* 16. Вывести модель самолета и список мест в нем, т.е. на самолет одна строка результата */
select a.model, string_agg(s.seat_no, ', ') as Список_мест
from bookings.aircrafts a 
	 join bookings.seats s on s.aircraft_code = a.aircraft_code
group by a.model;

/* 17. Вывести информацию по всем рейсам из аэропортов в г.Москва за сентябрь 2017 */
select * 
from bookings.flights f
	 join bookings.airports a on a.airport_code = f.departure_airport 
where EXTRACT(year FROM f.actual_departure) = 2016 --2017 года нет в базе, взял по 2016
	  and EXTRACT(month FROM f.actual_departure) = 9
	  and a.city = 'Москва'
	  				 			 
/* 18. Вывести кол-во рейсов по каждому аэропорту в г.Москва за 2017 */
select a.airport_name, count(f.flight_id) as Количество_рейсов 
from bookings.airports a
	 join bookings.flights f on f.departure_airport = a.airport_code 
where a.city = 'Москва'
	  and EXTRACT(year FROM f.actual_departure) = 2016 --2017 года нет в базе, взял по 2016
group by a.airport_name; 

/* 19. Вывести кол-во рейсов по каждому аэропорту, месяцу в г.Москва за 2017 */
select a.airport_name, 
	   EXTRACT(month FROM f.actual_departure) as Месяц, 
	   count(f.flight_id) as Количество_рейсов 
from bookings.airports a
	 join bookings.flights f on f.departure_airport = a.airport_code 
where a.city = 'Москва'
	  and EXTRACT(year FROM f.actual_departure) = 2016 --2017 года нет в базе, взял по 2016
group by a.airport_name, Месяц; 

/* 20. Найти все билеты по бронированию на «3A4B» */
select t.ticket_no, t.book_ref  
from bookings.tickets t 
where t.book_ref like '3A4B%'

/* 21. Найти все перелеты по бронированию на «3A4B» */
select f.flight_no, f.actual_departure, t.ticket_no, t.book_ref  
from bookings.tickets t
	 join bookings.ticket_flights tf on tf.ticket_no = t.ticket_no
	 join bookings.flights f on f.flight_id = tf.flight_id 
where t.book_ref like '3A4B%'
	  and f.actual_departure is not null 



