import java.util.Map;
import java.util.Random;

import static io.restassured.RestAssured.*;
import static org.junit.jupiter.api.Assertions.*;

import io.restassured.common.mapper.TypeRef;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;

import order.Endpoints;
import order.Order;

public class HomeTaskApiTest {

    @BeforeAll
    static void setUp() {
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri(Endpoints.host + Endpoints.orderBasePath) // задаём базовый адрес каждого ресурса
                .setAccept(ContentType.JSON) // задаём заголовок accept // приходит
                .setContentType(ContentType.JSON) // задаём заголовок content-type // когда мы отпр
                .log(LogDetail.ALL) // дополнительная инструкция полного логгирования для RestAssured
                .build(); // после этой команды происходит формирование стандартной "шапки" запроса.

        //Здесь задаётся фильтр, позволяющий выводить содержание ответа,
        // также к нему можно задать условия в параметрах конструктора, которм должен удовлетворять ответ (например код ответа)
        RestAssured.filters(new ResponseLoggingFilter());
    }

    //Для выбора рандомного значения
    private Order dummyOrder() {
        Order order = new Order();
        //order.setPetId((int) System.currentTimeMillis());
        order.setPetId(new Random().nextInt(10000));
        order.setId((int) System.currentTimeMillis());
        order.setQuantity(new Random().nextInt(10));
        return order;
    }

    private void createOrder(Order order) {
        // given().spec(requestSpec) - можно использовать для конкретного запроса свою спецификацию
        given()
                .body(order)
                .when()
                .post(Endpoints.order)
                .then()
                .statusCode(200);
    }

    private void deleteOrder(int id) {
        given()
                .pathParam("id", id)
                .when()
                .delete(Endpoints.order + Endpoints.orderId)
                .then()
                .statusCode(200).extract().body().as(Order.class);
    }

    private Order getOrder(int id) {
        return given()
                .pathParam("id", id)
                .when().get(Endpoints.order + Endpoints.orderId)
                .then().assertThat()
                .statusCode(200).extract().body().as(Order.class);
    }

    private Order getInvalidOrder(int id) {
        return given()
                .pathParam("id", id)
                .when().get(Endpoints.order + Endpoints.orderId)
                .then().assertThat()
                .statusCode(404).extract().body().as(Order.class);
    }

    @Test
    void createOrderTest() {
        Order order = dummyOrder();
        // создаем заказ
        createOrder(order);
        // проверяем правильно ли он создался
        Order apiOrder = getOrder(order.getId());
        assertEquals(order, apiOrder);
    }

    @Test
    void orderGetTest() {
        Order order = dummyOrder();
        // готовим данные для теста
        createOrder(order);
        // проверяем получение заказа
        Order apiOrder = getOrder(order.getId());
        assertEquals(order, apiOrder);
    }

    @Test
    void deleteOrderTest() {
        Order order = dummyOrder();
        // готовим данные для теста
        createOrder(order);
        // удаляем заказ и проверяем что он удалился
        deleteOrder(order.getId());
        Order apiOrderCheck = getInvalidOrder(order.getId());
        assertNotEquals(order, apiOrderCheck);
    }

    @Test
    void inventoryTest() {
        // сохраняем тело ответа в виде Map
        Map<String, Integer> inventory = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .when()
                .get(Endpoints.inventory)
                .then()
                .extract().body()
                .as(new TypeRef<Map<String, Integer>>() {
                });
        // валидируем значение "sold"
        assertTrue(inventory.containsKey("sold"), "Inventory не содержит статус sold");
    }
}
