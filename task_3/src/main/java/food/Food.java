package food;

public class Food {
    private String name;
    private int satietyCost;

    public Food(String name) {
        this.name = name;
    }

    public String getFoodName() {
        return name;
    }

    public int getSatietyCost() {
        return satietyCost;
    }

    public void setSatietyCost(int satietyCost) {
        this.satietyCost = satietyCost;
    }
}
