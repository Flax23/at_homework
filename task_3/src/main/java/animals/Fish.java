package animals;

import aviary.AviarySize;

public final class Fish extends Carnivorous implements Swim {
    public Fish(String name, AviarySize requiredAviarySize) {
        super(name, requiredAviarySize);
    }

    //Реализуем методы поведения в интерфейсах
    public void swim() {
        System.out.println(name + " плавает в озере");
    }
}
