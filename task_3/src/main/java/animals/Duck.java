package animals;

import aviary.AviarySize;

public final class Duck extends Herbivore implements Run, Fly, Swim, Voice {

    public Duck(String name, AviarySize requiredAviarySize) {
        super(name, requiredAviarySize);
    }

    //Реализуем методы поведения в интерфейсах
    public void swim() {
        System.out.println(name + " плавает в озере");
    }

    public void run() {
        System.out.println(name + " бежит по полю");
    }

    public void fly() {
        System.out.println(name + " летит над озером");
    }

    public String voice() {
        return name + " говорит кря-кря";
    }
}
