package animals;

public interface Fly {
    // Интерфейс может быть только абстрактым, переопределим метод в конкретных животных
    void fly();
}
