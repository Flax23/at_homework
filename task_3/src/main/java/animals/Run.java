package animals;

public interface Run {
    // Интерфейс может быть только абстрактым, переопределим метод в конкретных животных
    void run();
}
