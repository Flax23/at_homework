package animals;

import aviary.AviarySize;

public final class Elephant extends Herbivore implements Run, Voice {
    public Elephant(String name, AviarySize requiredAviarySize) {
        super(name, requiredAviarySize);
    }

    //Реализуем методы поведения в интерфейсах
    public void run() {
        System.out.println(name + " бежит за стадом");
    }

    public String voice() {
        return name + " громко кричит";
    }
}
