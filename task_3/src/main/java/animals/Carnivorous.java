package animals;

import aviary.AviarySize;
import food.Food;
import food.Meat;
import food.WrongFoodException;

public class Carnivorous extends Animal {
    // Передаем параметр в супер класс
    public Carnivorous(String name, AviarySize requiredAviarySize) {
        super(name, requiredAviarySize);
    }

    // Переопределенный метод супер класса. Кормим животное
    @Override
    public void eat(Food food) throws WrongFoodException {
        if (!(food instanceof Meat)) {
            throw new WrongFoodException(name + " - хищник и не ест " + food.getFoodName());
        } else {
            setSatietyLevel(food.getSatietyCost());
            System.out.println(name + " съел " + food.getFoodName() + ", сытость = " + getSatietyLevel());
        }
    }
}
