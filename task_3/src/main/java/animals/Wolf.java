package animals;

import aviary.AviarySize;

public final class Wolf extends Carnivorous implements Run, Swim, Voice {
    public Wolf(String name, AviarySize requiredAviarySize) {
        super(name, requiredAviarySize);
    }

    //Реализуем методы поведения в интерфейсах
    public void run() {
        System.out.println(name + " бежит за стаей");
    }

    public void swim() {
        System.out.println(name + " переплывает озеро");
    }

    public String voice() {
        return name + " рычит";
    }
}
