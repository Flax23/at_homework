package animals;

public interface Swim {
    // Интерфейс может быть только абстрактым, переопределим метод в конкретных животных
    void swim();
}
