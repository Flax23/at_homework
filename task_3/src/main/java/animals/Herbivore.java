package animals;

import aviary.AviarySize;
import food.Food;
import food.Grass;
import food.WrongFoodException;

public class Herbivore extends Animal {
    // Передаем параметр в супер класс
    public Herbivore(String name, AviarySize requiredAviarySize) {
        super(name, requiredAviarySize);
    }

    // Переопределенный метод супер класса. Кормим животное
    @Override
    public void eat(Food food) throws WrongFoodException {
        if (!(food instanceof Grass)) {
            throw new WrongFoodException(name + " - травоядный и не ест " + food.getFoodName());
        } else {
            setSatietyLevel(food.getSatietyCost());
            System.out.println(name + " съел " + food.getFoodName() + ", сытость = " + getSatietyLevel());
        }
    }
}
