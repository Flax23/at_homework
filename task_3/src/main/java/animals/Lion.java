package animals;

import aviary.AviarySize;

public final class Lion extends Carnivorous implements Run, Swim, Voice {
    public Lion(String name, AviarySize requiredAviarySize) {
        super(name, requiredAviarySize);
    }

    //Реализуем методы поведения в интерфейсах
    public void run() {
        System.out.println(name + " бежит за добычей");
    }

    public void swim() {
        System.out.println(name + " переплывает реку");
    }

    public String voice() {
        return name + " сильно рычит";
    }
}
