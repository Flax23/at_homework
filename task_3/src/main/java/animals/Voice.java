package animals;

public interface Voice {
    // Интерфейс может быть только абстрактым, переопределим метод в конкретных животных
    String voice();
}