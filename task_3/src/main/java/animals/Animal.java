package animals;

import aviary.AviarySize;
import food.Food;
import food.WrongFoodException;

public abstract class Animal {
    protected String name;
    private int satietyLevel = 0;
    private AviarySize requiredAviarySize;

    // Конструктор с параметром name
    public Animal(String name, AviarySize requiredAviarySize) {
        this.name = name;
        this.requiredAviarySize = requiredAviarySize;
    }

    // Гетеры и сетеры
    public void setSatietyLevel(int satietyLevel) {
        this.satietyLevel += satietyLevel;
    }

    public String getName() {
        return name;
    }

    public int getSatietyLevel() {
        return satietyLevel;
    }

    public AviarySize getRequiredAviarySize() {
        return requiredAviarySize;
    }

    // Переопределяем метод в классах потомках: Carnivorous и Herbivore
    public abstract void eat(Food food) throws WrongFoodException;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Animal animal = (Animal) obj;
        return name.equals(animal.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

}
