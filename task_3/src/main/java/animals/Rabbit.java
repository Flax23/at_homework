package animals;

import aviary.AviarySize;

public final class Rabbit extends Herbivore implements Run, Voice {
    public Rabbit(String name, AviarySize requiredAviarySize) {
        super(name, requiredAviarySize);
    }

    //Реализуем методы поведения в интерфейсах
    public void run() {
        System.out.println(name + " бегает за морковкой");
    }

    public String voice() {
        return name + " пищит";
    }
}
