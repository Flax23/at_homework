package aviary;

import animals.Animal;

import java.util.HashSet;
import java.util.Optional;

public class Aviary<T extends Animal> {
    private HashSet<T> animals;
    private AviarySize size;

    public Aviary(AviarySize size) {
        animals = new HashSet<>();
        this.size = size;
    }

    public boolean canAccommodateAnimal(AviarySize animalSize) {
        return animalSize.ordinal() <= size.ordinal();
    }

    public void addAnimal(T animal) {
        if (!canAccommodateAnimal(animal.getRequiredAviarySize())) {
            System.out.println(animal.getName() + " не подходит по размеру");
            return;
        }
        animals.add(animal);
        System.out.println(animal.getName() + " подходит по размеру для этого вольера");
    }

    public void removeAnimalByName(String name) {
        animals.removeIf(animal -> animal.getName().equals(name));
    }

    public Optional<T> getAnimalByName(String name) {
        return animals.stream()
                .filter(animal -> animal.getName().equals(name))
                .findFirst();
    }
}
