package aviary;

public enum AviarySize {
    SMALL,
    MEDIUM,
    LARGE,
    EXTRA_LARGE
}
