import animals.Animal;
import animals.Voice;
import food.WrongFoodException;
import food.Food;

public class Worker {
    private String name;

    // Конструктор рабочего
    public Worker(String name) {
        this.name = name;
    }

    // Метод кормления животного с параметрами animal и food, вызовем метод eat() и передаем в качестве параметра еду
    public void feed(Animal animal, Food food) {
        System.out.println(name + " кормит животное по имени - " + animal.getName());
        try {
            animal.eat(food);
        } catch (WrongFoodException e) {
            System.out.println("Ошибка: " + e.getMessage());
        }

    }

    // Выводим голос животного, в качестве параметра передаем объект типа Voice
    public void getVoice(Voice animal) {
        System.out.println(animal.voice());
    }

}
