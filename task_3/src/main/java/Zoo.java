import animals.*;
import aviary.Aviary;
import aviary.AviarySize;
import food.*;
import java.util.Optional;

public class Zoo {
    public static void main(String[] args) {
        // Создаем объекты: Рабочего, животных, еды
        var worker = new Worker("Рабочий");

        var meat = new Meat("кусок мяса");
        meat.setSatietyCost(2); // Утановим значение сытости для мяса
        var grass = new Grass("одуванчик");
        grass.setSatietyCost(1); // Установим значение сытости для травы

        var duck = new Duck("Кряква", AviarySize.SMALL);
        var elephant = new Elephant("Индийский слоник", AviarySize.EXTRA_LARGE);
        var lion = new Lion("Симба", AviarySize.LARGE);
        var wolf = new Wolf("Волк", AviarySize.MEDIUM);
        var fish = new Fish("Рыба", AviarySize.SMALL);
        var rabbit = new Rabbit("Кролик", AviarySize.SMALL);

        Aviary<Carnivorous> carnivoreAviary = new Aviary<>(AviarySize.LARGE);
        Aviary<Herbivore> herbivoreAviary = new Aviary<>(AviarySize.MEDIUM);

        carnivoreAviary.addAnimal(lion);
        carnivoreAviary.addAnimal(wolf);
        carnivoreAviary.addAnimal(fish);

        herbivoreAviary.addAnimal(duck);
        herbivoreAviary.addAnimal(elephant);
        herbivoreAviary.addAnimal(rabbit);

        Optional<Carnivorous> lion1 = carnivoreAviary.getAnimalByName("Симба");
        System.out.println("Симба: " + lion1.isPresent()); // Выводит "Симба: true"
        herbivoreAviary.removeAnimalByName("Кряква");

        worker.feed(lion, meat);
        worker.feed(lion, grass);
        worker.feed(fish, meat);
        worker.feed(fish, grass);
        worker.feed(elephant, grass);
        worker.feed(elephant, meat);

    }
}
