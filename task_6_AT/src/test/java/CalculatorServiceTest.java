import console.calculator.CalculatorService;
import console.calculator.ConsoleCalculator;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.annotations.Listeners;

@Listeners(TestListener.class)
public class CalculatorServiceTest {
    private CalculatorService calculator;

    @BeforeTest
    public void setUp() {
        calculator = new ConsoleCalculator();
    }

    @DataProvider(name = "validInputs")
    public Object[][] getValidInputs() {
        return new Object[][]{
                {"", "10", "2", 5.0},
                {"", "-10", "2", -5.0},
                {"", "-2", "-10", 0.2},
                {"", "-10.5", "3.5", -3.0},
                {"", "0", "3", 0}
        };
    }

    @DataProvider(name = "invalidInputs")
    public Object[][] getInvalidInputs() {
        return new Object[][]{
                {"", "10", "a"},
                {"", "b", "2"},
                {"", "", "abc"},
                {"", "", ""},
                {"", null, "2"}
        };
    }

    @DataProvider(name = "divideByZero")
    public Object[][] getDivideByZeroInputs() {
        return new Object[][]{
                {"", "10", "0"},
                {"", "-10", "0"},
                {"", "0", "0"}
        };
    }

    @Test(dataProvider = "validInputs")
    public void testValidInputs(String prompt, String d1, String d2, double expected) {
        double actual = calculator.divideTwoDigits(prompt, d1, d2);
        Assert.assertEquals(actual, expected);
    }

    @Test(dataProvider = "invalidInputs", expectedExceptions = IllegalArgumentException.class)
    public void testInvalidInputs(String prompt, String d1, String d2) {
        calculator.divideTwoDigits(prompt, d1, d2);
    }

    @Test(dataProvider = "divideByZero", expectedExceptions = ArithmeticException.class)
    public void testDivideByZero(String prompt, String d1, String d2) {
        calculator.divideTwoDigits(prompt, d1, d2);
    }
}

