package console.calculator;

import java.util.Scanner;

public class ConsoleCalculator implements CalculatorService {
    private final Scanner scanner;

    public ConsoleCalculator() {
        this.scanner = new Scanner(System.in);
    }

    @Override
    public void readTwoDigitsAndDivide(String prompt) {
        System.out.println(prompt);
        String d1Str = scanner.nextLine();
        System.out.println(prompt.replaceAll("первого", "второго"));
        String d2Str = scanner.nextLine();
        double result = divideTwoDigits(prompt, d1Str, d2Str);
        System.out.println("Результат: " + result);
    }

    @Override
    public double divideTwoDigits(String prompt, String d1Str, String d2Str) {
        if (d1Str == null || d2Str == null) {
            throw new IllegalArgumentException("Входные числа не могут быть null");
        }

        double num1, num2;
        try {
            num1 = Double.parseDouble(d1Str);
            num2 = Double.parseDouble(d2Str);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Неверный формат числа");
        }

        if (num2 == 0) {
            throw new ArithmeticException("На ноль делить нельзя!");
        }

        return num1 / num2;
    }
}
